#include "GameTime.h"
using namespace godot;



void GameTime::_register_methods()
{
    register_method("_init", &GameTime::_init);
    register_method("_ready", &GameTime::_ready);
    register_method("_input", &GameTime::_input);
    register_method("_process", &GameTime::_process);
    register_method("process_input", &GameTime::process_input);
    register_method("game_over", &GameTime::game_over);

    //register_property("GameTime_TYPE", &GameTime::GameTime_TYPE, 0);
}
void GameTime::_init()
{   
}

void GameTime::_ready()
{   
    get_node("Timer")->connect("timeout", this, "game_over");
}

void GameTime::_process(float delta)
{
    Timer* currTime = (Timer*)get_node("Timer");
    int percentLeft = currTime->get_time_left() / 60 * 100;
    set_value(percentLeft);
    //process_input(delta);
}

void GameTime::_input(Ref<InputEvent> event)
{
    
}

void GameTime::process_input(float delta)
{
    /*if (Input::get_singleton()->is_action_just_pressed("ui_cancel")) {
        if (Input::get_singleton()->get_mouse_mode() == Input::MOUSE_MODE_VISIBLE) {
            Input::get_singleton()->set_mouse_mode(Input::MOUSE_MODE_CAPTURED);
        }
        else {
            Input::get_singleton()->set_mouse_mode(Input::MOUSE_MODE_VISIBLE);
        }
    }*/
}

void GameTime::game_over()
{
    Godot::print("Game over!");
}
