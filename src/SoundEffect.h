#ifndef SoundEffect_H
#define SoundEffect_H


#include <Godot.hpp>
#include <AudioStreamPlayer2D.hpp>
#include <InputEvent.hpp>
#include <Input.hpp>

namespace godot {

    class SoundEffect : public AudioStreamPlayer2D
    {
        GODOT_CLASS(SoundEffect, AudioStreamPlayer2D);

        void process_input(float delta);
        void play_sfx();

        bool muted = true;
        real_t current_volume = 0;

        String sfx_toggle;
        String sfx_play;

    public:
        static void _register_methods();
        void _init();
        void _ready();
        void _input(Ref<InputEvent> event);
        void _process(float delta);

        
    };

}


#endif