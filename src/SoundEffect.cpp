#include "SoundEffect.h"
using namespace godot;



void SoundEffect::_register_methods()
{
    register_method("_init", &SoundEffect::_init);
    register_method("_ready", &SoundEffect::_ready);
    register_method("_input", &SoundEffect::_input);
    register_method("_process", &SoundEffect::_process);
    register_method("process_input", &SoundEffect::process_input);

    register_property("sfx_toggle", &SoundEffect::sfx_toggle, String("toggle"));
    register_property("sfx_play", &SoundEffect::sfx_play, String("play"));
}
void SoundEffect::_init()
{   
    Godot::print("SoundEffect set up");
}

void SoundEffect::_ready()
{   
    current_volume = get_volume_db();
    Godot::print("SoundEffect set up");
}

void SoundEffect::_process(float delta)
{
    process_input(delta);

}

void SoundEffect::_input(Ref<InputEvent> event)
{
    if(Input::get_singleton()->is_action_just_pressed("global_sfx_toggle") || Input::get_singleton()->is_action_just_pressed(sfx_toggle))
    {
        Godot::print(sfx_toggle);
        set_stream_paused(muted);
        muted = !muted;
    }

    if(Input::get_singleton()->is_action_just_pressed(sfx_play))
    {
        Godot::print(sfx_play);
        play_sfx();
    }

    /*if(Input::get_singleton()->is_action_just_pressed("sfx1_decrease"))
    {
        Godot::print("SoundEffect decreased");
        if(current_volume >= -70)
        {
            current_volume -= 10;
            set_volume_db(current_volume);
        }
    }

    if(Input::get_singleton()->is_action_just_pressed("sfx1_increase"))
    {
        Godot::print("SoundEffect increased");
        if (current_volume <= 14)
        {
            current_volume += 10;
            set_volume_db(current_volume);
        }
    */
}

void SoundEffect::play_sfx()
{
    play(0);
}

void SoundEffect::process_input(float delta)
{
    /*if (Input::get_singleton()->is_action_just_pressed("ui_cancel")) {
        if (Input::get_singleton()->get_mouse_mode() == Input::MOUSE_MODE_VISIBLE) {
            Input::get_singleton()->set_mouse_mode(Input::MOUSE_MODE_CAPTURED);
        }
        else {
            Input::get_singleton()->set_mouse_mode(Input::MOUSE_MODE_VISIBLE);
        }
    }*/

}
