#ifndef BackgroundMusic_H
#define BackgroundMusic_H


#include <Godot.hpp>
#include <AudioStreamPlayer2D.hpp>
#include <InputEvent.hpp>
#include <Input.hpp>

namespace godot {

    class BackgroundMusic : public AudioStreamPlayer2D
    {
        GODOT_CLASS(BackgroundMusic, AudioStreamPlayer2D);

        void process_input(float delta);
        bool muted = false;
        real_t current_volume;
    public:
        static void _register_methods();
        void _init();
        void _ready();
        void _input(Ref<InputEvent> event);
        void _process(float delta);
    };

}


#endif