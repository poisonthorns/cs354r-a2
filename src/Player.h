#ifndef PLAYER_H
#define PLAYER_H


#include <Godot.hpp>
#include <KinematicBody.hpp>
#include <KinematicCollision.hpp>
#include <InputEventMouseMotion.hpp>
#include <Camera.hpp>
#include <Reference.hpp>
#include <InputEvent.hpp>
#include <Input.hpp>
#include <cstdlib>
#include <string>
#include <RayCast.hpp>
#include <Area.hpp>
#include <Object.hpp>
#include <AudioStreamPlayer2D.hpp>

namespace godot {

    class Player : public KinematicBody
    {
        GODOT_CLASS(Player, KinematicBody);
        Vector3 vel = Vector3();
        Vector3 dir = Vector3();

        Camera camera;
        Spatial rotation_helper;
        Spatial player_rotate;
        RayCast camRay;

        bool CRM = true; //camera rotate mode

        float defaultDeccel;
        float airDeccel;
        float defaultAccel;
        float airAccel;
        float defaultGravity;
        float glideGravity;
        
        float sprintSpeed;
        int curStamina;
        Object *tempInvis;

        void process_input(float delta);
        void process_movement(float delta);

    public:
        float defaultSpeed;

        float GRAVITY = -24.8f;
        float MAX_SPEED = 20.0f;
        float JUMP_SPEED = 18.0f;
        float ACCEL = 4.5f;
        float DEACCEL= 16.0f;
        float ROTATE_SPEED = 0.03f;
        float AIR_RESIST = .95f; // how much the player resists changind direction in air
        float AIR_CONTROL = .6f; // 0 - 1 for percentage of control in air
        float MAX_SLOPE_ANGLE = 40.0f;
        float MOUSE_SENSITIVITY = 0.05f;  
        float GLIDE_SLOW = 0.9f;  
        int STAMINA = 100;
        int STAMINA_DRAIN =1;
        int SPRINT_MULTIPLIER = 3;

        static void _register_methods();
        void _init();
        void _ready();
        void _physics_process(float delta);
        void _process(float delta);
        void _input(Ref<InputEvent> event);
    };

}

#endif