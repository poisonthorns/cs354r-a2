#include "Interactable.h"
using namespace godot;



void Interactable::_register_methods()
{
    register_method("_init", &Interactable::_init);
    register_method("_ready", &Interactable::_ready);
    register_method("_input", &Interactable::_input);
    register_method("_process", &Interactable::_process);
    register_method("process_input", &Interactable::process_input);

    register_property("INTERACTABLE_TYPE", &Interactable::INTERACTABLE_TYPE, 0);
}
void Interactable::_init()
{   
}

void Interactable::_ready()
{   

}

void Interactable::_process(float delta)
{
    process_input(delta);
}

void Interactable::_input(Ref<InputEvent> event)
{
    
}

void Interactable::process_input(float delta)
{
    /*if (Input::get_singleton()->is_action_just_pressed("ui_cancel")) {
        if (Input::get_singleton()->get_mouse_mode() == Input::MOUSE_MODE_VISIBLE) {
            Input::get_singleton()->set_mouse_mode(Input::MOUSE_MODE_CAPTURED);
        }
        else {
            Input::get_singleton()->set_mouse_mode(Input::MOUSE_MODE_VISIBLE);
        }
    }*/
}

void Interactable::effect(Player* player)
{

    if(this->get_name() == "TheOrb"){
        Godot::print("the orb set");
    }
    if(this->get_name() == "Speed"){
        Godot::print("speed set");
    }
    if(this->get_name() == "Jump"){
        Godot::print("jump set");
    }
    //Godot::print("I am here");
    //player->defaultSpeed = 80;
}
