#ifndef Interactable_H
#define Interactable_H


#include <Godot.hpp>
#include <Area.hpp>
#include <InputEvent.hpp>
#include <Input.hpp>
#include <Reference.hpp>
#include <Player.h>

namespace godot {

    class Interactable : public Area
    {
        GODOT_CLASS(Interactable, Area);
        String name;
        void process_input(float delta);

    public:
        static void _register_methods();
        void _init();
        void _ready();
        void _input(Ref<InputEvent> event);
        void _process(float delta);

        int INTERACTABLE_TYPE = 0;

        void effect(Player* player);

    };

}


#endif