#include "Player.h"
using namespace godot;
#include <Interactable.h>


void Player::_register_methods()
{
    register_method("_init", &Player::_init);
    register_method("_ready", &Player::_ready);
    register_method("_process", &Player::_process);
    register_method("_physics_process", &Player::_physics_process);
    register_method("_input", &Player::_input);
    register_method("process_input", &Player::process_input);
    register_method("process_movement", &Player::process_movement);
    
    register_property("Gravity", &Player::GRAVITY, -24.8f);
	register_property("Max Speed", &Player::MAX_SPEED, 20.0f);
	register_property("Jump Speed", &Player::JUMP_SPEED, 18.0f);
	register_property("Acceleration", &Player::ACCEL, 4.5f);
	register_property("Deceleration", &Player::DEACCEL, 16.0f);
	register_property("Max Slope Angle", &Player::MAX_SLOPE_ANGLE, 40.0f);
	register_property("Mouse Sensitivty", &Player::MOUSE_SENSITIVITY, 0.05f);
    register_property("Rotate Speed", &Player::ROTATE_SPEED, 0.03f);
    register_property("Camera Rotate Mode", &Player::CRM, true);
    register_property("Air Resistance", &Player::AIR_RESIST, 0.95f);
    register_property("Air Control", &Player::AIR_CONTROL, 0.6f);
    register_property("Glide Slow", &Player::GLIDE_SLOW, 0.9f);
    register_property("Stamina", &Player::STAMINA, 100);
    register_property("Sprint Drain", &Player::STAMINA_DRAIN, 1);
    register_property("Sprint Speed Multiplier", &Player::SPRINT_MULTIPLIER, 3);
}
void Player::_init()
{   
}

void Player::_ready()
{   
    camera = *((Camera*)get_node("Rotation_Helper/Camera"));
    rotation_helper = *((Spatial*)get_node("Rotation_Helper"));
    player_rotate = *((Spatial*)get_node("../Player"));
    camRay = *((RayCast*)get_node("RayCast"));
    Input::get_singleton()->set_mouse_mode(Input::MOUSE_MODE_CAPTURED);
    defaultGravity = GRAVITY;
    glideGravity = GRAVITY * (1-GLIDE_SLOW);
    defaultDeccel = DEACCEL;
    airDeccel = DEACCEL * (1-AIR_RESIST);
    defaultAccel = ACCEL;
    airAccel = ACCEL * AIR_CONTROL;
    defaultSpeed = MAX_SPEED;
    sprintSpeed = MAX_SPEED * SPRINT_MULTIPLIER;
    curStamina = STAMINA;
}

void Player::_process(float delta)
{   
    if(Input::get_singleton()->is_action_just_pressed("interact"))
    {
        //Godot::print("Called");
        Area playerArea = *((Area*)get_node("Area"));
        //Godot::print("After get node");
        Array collisions = playerArea.get_overlapping_areas();
        //Godot::print("After get overlap");
        if (collisions.size() > 0)
        {
            Area *firstCollision = (Area*)collisions[0];
            //Godot::print("After cast");

            //Godot::print("After if before get name");
            String name = firstCollision->get_name();
            //Interactable* interactable = (Interactable*)firstCollision->get_script();
            //interactable->effect(this);

            if(name == "TheOrb"){
                AudioStreamPlayer2D sfx = *((AudioStreamPlayer2D*)get_node("OrbSFXPlayer"));
                sfx.play();
                Godot::print("THE ORB IS PICKED UP");
            }
            if(name == "Speed"){
                AudioStreamPlayer2D sfx = *((AudioStreamPlayer2D*)get_node("SpeedSFXPlayer"));
                sfx.play();
                defaultSpeed = MAX_SPEED * 4;
            }
            if(name == "Jump"){
                AudioStreamPlayer2D sfx = *((AudioStreamPlayer2D*)get_node("JumpSFXPlayer"));
                sfx.play();
                JUMP_SPEED = JUMP_SPEED * 2;
            }

        }
        else
        {
            AudioStreamPlayer2D sfx = *((AudioStreamPlayer2D*)get_node("FailedSFXPlayer"));
            sfx.play();
            Godot::print("No collision");
        }
    }
}

void Player::_physics_process(float delta) 
{
    process_input(delta);
    process_movement(delta);
    if(camRay.is_enabled() && camRay.is_colliding()){
        Object *collider = camRay.get_collider();
        collider->set("visible", false);
        tempInvis = collider;
    } else {
        if(tempInvis != NULL) {
            tempInvis->set("visible", true);
            tempInvis = NULL;
        }
    }
}

void Player::_input(Ref<InputEvent> event) 
{
    // orig line: if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
    if(CRM && event->get_class() == "InputEventMouseMotion" && Input::get_singleton()->get_mouse_mode() == Input::MOUSE_MODE_CAPTURED)
    {
        Godot::print(event->get_class() == "InputEventMouseMotion");
        Ref<InputEventMouseMotion> mouse_event = (Ref<InputEventMouseMotion>)event;

        //rotation_helper.rotate_x(Math::deg2rad(mouse_event->get_relative().y * MOUSE_SENSITIVITY));
        rotate_y(Math::deg2rad(-mouse_event->get_relative().x * MOUSE_SENSITIVITY));

        Vector3 camera_rot = rotation_helper.get_rotation_degrees();
        //camera_rot.x = CLAMP(camera_rot.x, -70, 70);
        //should serve the perpose of "CLAMP"
        camera_rot.x = (camera_rot.x < -70) ? -70 : ((camera_rot.x > 70) ? 70 : camera_rot.x);
        rotation_helper.set_rotation_degrees(camera_rot);
    }
}


void Player::process_input(float delta)
{
    //  Walking
    dir = Vector3();
    Transform camXform = camera.get_global_transform();

    Vector2 inputMovementVector = Vector2();
    if(is_on_floor()){
        ACCEL = defaultAccel;
        DEACCEL = defaultDeccel;
        if(Input::get_singleton()->is_action_pressed("movement_sprint") && curStamina > 0)
        {
            printf("%d", curStamina);
            MAX_SPEED = sprintSpeed;
            curStamina -= STAMINA_DRAIN;
            if(curStamina == 0){
                curStamina -= 30;
            }
            
        } else {
            MAX_SPEED = defaultSpeed;
            if(curStamina < STAMINA) {
                curStamina++;
            }
        }
    } else {
        ACCEL = airAccel;
        DEACCEL = airDeccel;
        if(Input::get_singleton()->is_action_pressed("movement_jump") && vel.y < 0)
        {
            GRAVITY = glideGravity;
        } else {
            GRAVITY = defaultGravity;
        }
    }


    if(Input::get_singleton()->is_action_pressed("movement_forward"))
    {
        inputMovementVector.y += 1;
    }
    if(Input::get_singleton()->is_action_pressed("movement_backward"))
    {
        inputMovementVector.y -= 1;
    }
    if(CRM && Input::get_singleton()->is_action_pressed("movement_left"))
    {
        inputMovementVector.x -= 1;
    } 
    if(!CRM && Input::get_singleton()->is_action_pressed("movement_left")) 
    {
        player_rotate.rotate_y(ROTATE_SPEED);
    }
    if(CRM && Input::get_singleton()->is_action_pressed("movement_right"))
    {
        inputMovementVector.x += 1;
    } 
    if (!CRM && Input::get_singleton()->is_action_pressed("movement_right")) 
    {
        player_rotate.rotate_y(-ROTATE_SPEED);
    }

    inputMovementVector = inputMovementVector.normalized();

    // // Basis vectors are already normalized.
    dir += -camXform.basis.z * inputMovementVector.y;
    dir += camXform.basis.x * inputMovementVector.x;

    
    // //  Jumping
    if(is_on_floor())
    {
        if(Input::get_singleton()->is_action_just_pressed("movement_jump"))
            vel.y = JUMP_SPEED;
    }


    if (Input::get_singleton()->is_action_just_pressed("ui_cancel")) {
        if (Input::get_singleton()->get_mouse_mode() == Input::MOUSE_MODE_VISIBLE) {
            Input::get_singleton()->set_mouse_mode(Input::MOUSE_MODE_CAPTURED);
        }
        else {
            Input::get_singleton()->set_mouse_mode(Input::MOUSE_MODE_VISIBLE);
        }
    }
}

void Player::process_movement(float delta)
{
    dir.y = 0;
    dir = dir.normalized();

    vel.y += delta * GRAVITY;

    Vector3 hvel = vel;
    hvel.y = 0;

    Vector3 target = dir;

    target *= MAX_SPEED;

    float accel;
    if(dir.dot(hvel) > 0)
        accel = ACCEL;
    else
        accel = DEACCEL;

    hvel = hvel.linear_interpolate(target, accel * delta);
    vel.x = hvel.x;
    vel.z = hvel.z;
    vel = move_and_slide(vel, Vector3(0, 1, 0), false, 4, Math::deg2rad(MAX_SLOPE_ANGLE));
}
