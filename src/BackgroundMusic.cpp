#include "BackgroundMusic.h"
using namespace godot;



void BackgroundMusic::_register_methods()
{
    register_method("_init", &BackgroundMusic::_init);
    register_method("_ready", &BackgroundMusic::_ready);
    register_method("_input", &BackgroundMusic::_input);
    register_method("_process", &BackgroundMusic::_process);
    register_method("process_input", &BackgroundMusic::process_input);

    //register_property("Gravity", &BackgroundMusic::GRAVITY, -24.8f);
}
void BackgroundMusic::_init()
{   
    Godot::print("BackgroundMusic set up");
}

void BackgroundMusic::_ready()
{   
    current_volume = get_volume_db();
    Godot::print("BackgroundMusic set up");
}

void BackgroundMusic::_process(float delta)
{
    process_input(delta);

}

void BackgroundMusic::_input(Ref<InputEvent> event)
{
    if(Input::get_singleton()->is_action_just_pressed("background_music_toggle"))
    {
        Godot::print("BackgroundMusic muted");
        set_stream_paused(!muted);
        muted = !muted;
    }

    if(Input::get_singleton()->is_action_just_pressed("decrease_background_music"))
    {
        Godot::print("BackgroundMusic decreased");
        if(current_volume >= -70)
        {
            current_volume -= 10;
            set_volume_db(current_volume);
        }
    }

    if(Input::get_singleton()->is_action_just_pressed("increase_background_music"))
    {
        Godot::print("BackgroundMusic increased");
        if (current_volume <= 14)
        {
            current_volume += 10;
            set_volume_db(current_volume);
        }
    }
}

void BackgroundMusic::process_input(float delta)
{
    /*if (Input::get_singleton()->is_action_just_pressed("ui_cancel")) {
        if (Input::get_singleton()->get_mouse_mode() == Input::MOUSE_MODE_VISIBLE) {
            Input::get_singleton()->set_mouse_mode(Input::MOUSE_MODE_CAPTURED);
        }
        else {
            Input::get_singleton()->set_mouse_mode(Input::MOUSE_MODE_VISIBLE);
        }
    }*/

}
