#ifndef GameTime_H
#define GameTime_H


#include <Godot.hpp>
#include <TextureProgress.hpp>
#include <InputEvent.hpp>
#include <Input.hpp>
#include <Reference.hpp>
#include <Player.h>
#include <Timer.hpp>

namespace godot {

    class GameTime : public TextureProgress
    {
        GODOT_CLASS(GameTime, TextureProgress);

        void process_input(float delta);
       

    public:
        static void _register_methods();
        void _init();
        void _ready();
        void _input(Ref<InputEvent> event);
        void _process(float delta);
        void GameTime::game_over();

    };

}


#endif