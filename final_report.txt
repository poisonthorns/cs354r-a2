Final Report:

Our game idea was developed looking forward at Project 3 involving setting up multiplayer networked play, so we designed a concept for a multiplayer game. We think we did come up with a genuinely interesting, and hopefully fun, concept, and we're excited to continue working on it.

Successes:
We were able to achieve most of the design objectives set out in the requirements. The player character moves, jumps, glides, and sprints fluidly, and the camera works perfectly, including being able to hide geometry between the camera and the player. Just moving around proved to feel better than we expected, and getting the camera to work properly took some effort. 

We were able to obtain nice sound effects and background music thanks to Skylore having had access to these resources, and they definitely enhanced the feel of the game, even if nothing changed mechanically.

We were generally able to keep the code layout organized, commented, and simple, there was never much agonizing or questioning for how anything worked, between us all.





Issues:
Working on the project remotely and with team members who were running different OS's meant we lost a lot of time just trying to deal with compatibility issues, and getting code to compile. Resolving this would be a matter of understanding better what goes on between platforms, and what work can be done and transferred easily between OS's.

Lack of time and being stymied by approach also made it such that we were unable to implement things such as the 2 ledge interactions other than falling. This would be solvable by better managing time and starting earlier, for future projects.

In general we had to scale back concepts to try and make things simpler, such as changing the custom movement from a roll (which would require manipulating the player model) to a sprint with a timer (which would not require animation changes).